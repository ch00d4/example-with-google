FROM python:3.10
# RUN apk add postgresql-dev gcc python3-dev musl-dev
ENV PYTHONUNBUFFERED 1
COPY . /code/
WORKDIR /code/
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
EXPOSE 8080
CMD ["gunicorn", "--bind", "0.0.0.0:8080", "core.wsgi:application"]
