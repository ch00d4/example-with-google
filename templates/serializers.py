from rest_framework import serializers

from tags.serializers import TagModelSeralizer

from .models import Template
from users.serializers import UserSimpleModelSerializer


class TemplateModelSerializer(serializers.ModelSerializer):
    created_by = UserSimpleModelSerializer(many=False, read_only=True)
    updated_by = UserSimpleModelSerializer(many=False, read_only=True)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['category'] = TagModelSeralizer(instance.category, many=True).data
        return data

    def create(self, validated_data):
        validated_data['created_by'] = self.context['request'].user
        validated_data['updated_by'] = self.context['request'].user
        return super().create(validated_data)

    def update(self, instance, validated_data):
        validated_data['updated_by'] = self.context['request'].user
        return super().update(instance, validated_data)

    class Meta:
        model = Template
        fields = ('id', 'name', 'description', 'content', 'category',
                  'created_by', 'updated_by', 'date_created', 'date_updated')
        extra_kwargs = {
            'created_by': {'read_only': True},
            'updated_by': {'read_only': True},
            'date_created': {'read_only': True},
            'date_updated': {'read_only': True},
            'description': {'required': False},
            'content': {'required': False},
            'category': {'required': False},
        }


class TemplateResponseModelSerializer(serializers.ModelSerializer):
    category = TagModelSeralizer(many=True, read_only=True)
    created_by = UserSimpleModelSerializer(many=False, read_only=True)
    updated_by = UserSimpleModelSerializer(many=False, read_only=True)

    class Meta:
        model = Template
        fields = ('id', 'name', 'description', 'content', 'category',
                  'created_by', 'updated_by', 'date_created', 'date_updated')
