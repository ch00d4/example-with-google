from django.urls import path

from rest_framework import routers

from .views import TemplateModelViewSet

ROUTER = routers.SimpleRouter()
ROUTER.register('', TemplateModelViewSet)

urlpatterns = []

urlpatterns += ROUTER.urls
