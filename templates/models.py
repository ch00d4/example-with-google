from django.db import models

from tags.models import Tag
from users.models import User


class Template(models.Model):
    name = models.TextField(unique=True)
    description = models.TextField(blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    category = models.ManyToManyField(Tag, blank=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='created_template')
    updated_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updated_template')
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'template'
