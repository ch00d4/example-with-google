from django_filters.rest_framework import DjangoFilterBackend
from drf_spectacular.utils import extend_schema
from rest_framework import filters, viewsets

from .models import Template
from .permissions import TemplatePermissions
from .serializers import (
    TemplateModelSerializer,
    TemplateResponseModelSerializer
)


class TemplateModelViewSet(viewsets.ModelViewSet):
    serializer_class = TemplateModelSerializer
    queryset = Template.objects.all()
    permission_classes = (TemplatePermissions, )
    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    filterset_fields = ['category', ]
    search_fields = ['name', ]

    @extend_schema(
        responses=TemplateResponseModelSerializer
    )
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @extend_schema(
        responses=TemplateResponseModelSerializer
    )
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)
