from rest_framework import permissions

from groups.models import Group


class TagPermissions(permissions.BasePermission):

    def has_permission(self, request, view):
        group = Group.objects.get(name='admin')

        if request.user.is_anonymous and view.action in ('create', 'update', 'partial_update', 'destroy'):
            return False

        if not request.user.is_anonymous and group in request.user.groups.all() and view.action in ('create', 'update', 'partial_update', 'destroy'):
            return True

        return True
