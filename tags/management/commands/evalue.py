from tags.models import Tag, TagExtended
from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):
    help = 'evalue TagExtends'
     
    def handle(self,*args,**options):
        tags = Tag.objects.all()
        for tag in tags: 
            try:
                tag_extendeds = TagExtended.objects.get(tag=tag)
            except:
                tag_extendeds = 'Tag does not have an extended registered'
            print({
                'tag':{
                    'name':tag.name, 'lvl':tag.lvl, 'nemonic':tag.nemonic, 'parents':tag.parents} , 'tagextended':tag_extendeds}) 
        self.stdout.write(self.style.SUCCESS('Successfully evaluation'))     
