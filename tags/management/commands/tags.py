from tags.models import Tag, TagExtended
from django.core.management.base import BaseCommand

class Command(BaseCommand):
    help = 'create tag extended'

    def handle(self,*args,**options) :
        tags = Tag.objects.all()
        for tag in tags: 
             tag_extendeds, created = TagExtended.objects.get_or_create(tag=tag) 
             print({'Tag':tag.name , 'created':created})

        self.stdout.write(self.style.SUCCESS('Successfully created tag extendeds'))         
 