from django.utils import timezone

from rest_framework import serializers

from .models import Tag, TagExtended
from .utils import generate_parents_names


class TagSimpleModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tag
        fields = ('id', 'name', 'description', 'nemonic', 'parents')


class TagModelSeralizer(serializers.ModelSerializer):
    parent = serializers.CharField(write_only=True, required=False, allow_blank=True, allow_null=True)
    childs = serializers.IntegerField(read_only=True)
    parents = serializers.CharField(read_only=True)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['nemonic'] = instance.nemonic
        tag_extended = TagExtended.objects.get(tag=instance)
        data['parent'] = tag_extended.parent.nemonic if tag_extended.parent is not None else None
        data['parents'] = generate_parents_names(instance, '')
        data['childs'] = TagExtended.objects.filter(parent=instance).count()
        return data

    def create(self, validated_data):
        parent = None
        try:
            parent = Tag.objects.get(nemonic=validated_data.pop('parent', None))
        except Tag.DoesNotExist:
            pass

        nemonic = timezone.now().strftime('%Y%m%d%H%M%S%f')

        if parent is None:
            tag = Tag.objects.create(lvl=0, nemonic=nemonic, **validated_data)
            TagExtended.objects.create(tag=tag, parent=None, primary=tag)
        else:
            tag = Tag.objects.create(lvl=parent.lvl + 1, nemonic=nemonic, **validated_data)
            extended = TagExtended.objects.get(tag=parent)
            if parent.lvl == 1:
                TagExtended.objects.create(tag=tag, parent=parent, primary=parent)

            else:
                TagExtended.objects.create(tag=tag, parent=parent, primary=extended.primary)

        return tag

    class Meta:
        model = Tag
        fields = ('id', 'name', 'description', 'parent', 'lvl', 'parents', 'nemonic', 'childs')
        extra_kwargs = {
            'lvl': {'read_only': True},
            'parents': {'read_only': True},
            'parent': {'required': False},
            'nemonic': {'read_only': True},
        }


class TagExtendedModelSerializer(serializers.ModelSerializer):

    def to_representation(self, instance):
        serializer = TagModelSeralizer(instance=instance.tag)
        return serializer.data

    class Meta:
        model = TagExtended
        fields = ('tag', )
