from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models


class Tag(models.Model):
    name = models.TextField()
    description = models.TextField()
    lvl = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(12)])
    nemonic = models.CharField(max_length=24, unique=True)
    parents = models.TextField(null=True, blank=True)


    class Meta:
        db_table = 'tag'


class TagExtended(models.Model):
    tag = models.OneToOneField(Tag, on_delete=models.CASCADE, null=False, related_name='tag')
    parent = models.ForeignKey(Tag, on_delete=models.CASCADE, null=True, related_name='parent_tag')
    primary = models.ForeignKey(Tag, on_delete=models.CASCADE, null=True, related_name='primary_tag')

    class Meta:
        db_table = 'tag_extended'
