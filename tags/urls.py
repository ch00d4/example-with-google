from django.urls import path

from rest_framework import routers

from .views import (
    ChildTagViewSet,
    ParentTagViewSet,
    TagModelViewSet,
)

ROUTER = routers.SimpleRouter()
ROUTER.register('', TagModelViewSet)

urlpatterns = [
    path('childs/', ChildTagViewSet.as_view({'get': 'list'})),
    path('parents/', ParentTagViewSet.as_view({'get': 'list'})),
]

urlpatterns += ROUTER.urls
