from django_filters.rest_framework import DjangoFilterBackend
from drf_spectacular.utils import OpenApiParameter, OpenApiTypes, extend_schema
from rest_framework import filters
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet, ModelViewSet

from .models import Tag, TagExtended
from .permissions import TagPermissions
from .serializers import TagExtendedModelSerializer, TagModelSeralizer


class TagModelViewSet(ModelViewSet):
    queryset = Tag.objects.all()
    serializer_class = TagModelSeralizer
    permission_classes = (TagPermissions, )
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    search_fields = ['name', 'description']
    filterset_fields = ['lvl', 'nemonic', 'primary_tag', 'tag__primary__nemonic']


class ChildTagViewSet(GenericViewSet):
    permission_classes = (AllowAny, )
    serializer_class = TagModelSeralizer
    pagination_class = None
    queryset = Tag.objects.none()

    @extend_schema(
        parameters=[
            OpenApiParameter('nemonic', OpenApiTypes.STR, OpenApiParameter.QUERY),
        ]
    )
    def list(self, request):
        nemonic = self.request.query_params.get('nemonic', '')
        tags = TagExtended.objects.filter(parent__nemonic=nemonic)
        serializer = TagExtendedModelSerializer(instance=tags, many=True)
        return Response(data=serializer.data)


class ParentTagViewSet(GenericViewSet):
    permission_classes = (AllowAny, )
    serializer_class = TagModelSeralizer
    pagination_class = None
    queryset = Tag.objects.none()

    @extend_schema(
        parameters=[
            OpenApiParameter('nemonic', OpenApiTypes.STR, OpenApiParameter.QUERY),
        ],
    )
    def list(self, request):
        nemonic = self.request.query_params.get('nemonic', '')
        tags = TagExtended.objects.filter(primary__nemonic=nemonic).exclude(tag__lvl=1)
        serializer = TagExtendedModelSerializer(instance=tags, many=True)
        return Response(data=serializer.data)
