from .models import TagExtended


def generate_parents_names(tag, parents):
    extended = TagExtended.objects.get(tag=tag)
    parents += f'{tag.name},'
    if extended.parent is not None:
        return generate_parents_names(extended.parent, parents)

    parents = parents.split(',')
    parents = list(reversed(parents))
    del parents[0]
    return ' / '.join(parents)
