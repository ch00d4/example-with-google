from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path

from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView

urlpatterns = [
    path('schema/', SpectacularAPIView.as_view(), name='schema'),
    path(
        '',
        SpectacularSwaggerView.as_view(
            template_name='swagger-ui.html', url_name='schema'
        ),
        name='swagger-ui',
    ),
    path('auth/', include('authentication.urls'), name='auth'),
    path('auth/', include('authentication.google.urls'), name='auth-google'),
    path('users/', include('users.urls'), name='users'),
    path('groups/', include('groups.urls'), name='groups'),
    path('profile/', include('profiles.urls'), name='profiles'),
    path('tags/', include('tags.urls'), name='tags'),
    path('templates/', include('templates.urls'), name='templates')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
