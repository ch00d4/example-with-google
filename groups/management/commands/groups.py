from django.conf import settings
from django.core.management.base import BaseCommand

from groups.models import Group


class Command(BaseCommand):
    help = 'add groups'

    def handle(self, *args, **options):
        groups = settings.CORE_GROUPS_DEFAULT
        for group in groups:
            _group, _ = Group.objects.get_or_create(id=group['id'], name=group['name'])

        self.stdout.write(self.style.SUCCESS('Successfully created groups'))
