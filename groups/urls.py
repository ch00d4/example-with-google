from django.urls import include, path

from rest_framework import routers

from .views import GroupsModelViewSet

router = routers.SimpleRouter()
router.register('', GroupsModelViewSet, 'groups')

urlpatterns = [
    path('', include(router.urls)),
]
