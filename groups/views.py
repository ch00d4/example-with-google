from django.contrib.auth.models import Group

from rest_framework import viewsets

from users.permissions import AdministratorPermissions

from .serializers import GroupModelSerializer


class GroupsModelViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupModelSerializer
    permission_classes = (AdministratorPermissions,)
    pagination_class = None
    http_method_names = ['get', ]
