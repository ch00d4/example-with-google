from django_filters.rest_framework import DjangoFilterBackend
from drf_spectacular.utils import extend_schema
from rest_framework import filters, viewsets

from .permissions import AdministratorPermissions
from .serializers import UserModelResponseSerializer, UserModelSerializer


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = (AdministratorPermissions, )
    serializer_class = UserModelSerializer
    queryset = UserModelSerializer.Meta.model.objects.all()
    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    filterset_fields = ['groups', ]
    search_fields = ['username', 'email', 'first_name', 'last_name']
    http_method_names = ['get', 'put', 'patch']

    @extend_schema(
        responses=UserModelResponseSerializer
    )
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @extend_schema(
        responses=UserModelResponseSerializer
    )
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)
