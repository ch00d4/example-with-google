from django.contrib.auth.models import User

from rest_framework import serializers

from groups.serializers import GroupModelSerializer


class UserModelSerializer(serializers.ModelSerializer):

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['groups'] = GroupModelSerializer(instance.groups, many=True).data
        return data

    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'last_name', 'groups')
        read_only_fields = ('id', 'is_superuser', 'email')


class UserModelResponseSerializer(serializers.ModelSerializer):
    groups = GroupModelSerializer(many=True, read_only=True)

    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'last_name', 'groups')
        read_only_fields = ('id', 'is_superuser', 'email')


class UserSimpleModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'last_name')
