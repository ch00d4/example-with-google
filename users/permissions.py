from rest_framework import permissions


class AdministratorPermissions(permissions.IsAdminUser):

    def has_permission(self, request, view):
        if request.method == 'DELETE' and not request.user.is_superuser:
            return False

        if request.user.is_anonymous:
            return False

        return True
