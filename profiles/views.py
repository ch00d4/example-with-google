from django.conf import settings

from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from groups.models import Group

from .models import Profile
from .serializers import AvatarSerializer, ProfileModelSerializer


class AvatarViewSet(GenericViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = AvatarSerializer

    def get(self, request):
        profile, _ = Profile.objects.get_or_create(user=request.user)
        serializer_class = self.get_serializer_class()
        serializer = serializer_class(profile, context={'request': request})
        return Response(serializer.data)

    def create(self, request):
        profile, _ = Profile.objects.get_or_create(user=request.user)
        serializer_class = self.get_serializer_class()
        serializer = serializer_class(profile, data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)


class ProfileViewSet(GenericViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = ProfileModelSerializer

    def get(self, request):
        profile, _ = Profile.objects.get_or_create(user=request.user)
        user = request.user
        if user.groups.all().count() == 0 and user.email not in settings.CORE_ADMINS_EMAIL:
            group = Group.objects.get(name='client')
            user.groups.add(group)
            user.save()

        if user.email in settings.CORE_ADMINS_EMAIL:
            group = Group.objects.get(name='admin')
            user.groups.add(group)
            user.save()

        serializer_class = self.get_serializer_class()
        serializer = serializer_class(profile, context={'request': request})
        return Response(serializer.data)

    def create(self, request):
        profile, _ = Profile.objects.get_or_create(user=request.user)
        serializer_class = self.get_serializer_class()
        serializer = serializer_class(profile, data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)
