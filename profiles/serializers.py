from rest_framework import serializers

from users.serializers import UserModelResponseSerializer

from .models import Profile


class AvatarSerializer(serializers.ModelSerializer):

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['avatar'] = self.context['request'].build_absolute_uri(
            instance.avatar.url) if instance.avatar else 'https://cdn.teranvo.com/public/avatar.png'
        return data

    class Meta:
        model = Profile
        fields = ('avatar',)


class ProfileModelSerializer(serializers.ModelSerializer):
    user = UserModelResponseSerializer(read_only=True)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['avatar'] = self.context['request'].build_absolute_uri(
            instance.avatar.url) if instance.avatar else 'https://cdn.teranvo.com/public/avatar.png'
        return data

    class Meta:
        model = Profile
        fields = ('user', 'phone', 'address', 'avatar')
        read_only_fields = ('user', 'avatar')
        depth = 1
