from django.urls import path

from .views import AvatarViewSet, ProfileViewSet

urlpatterns = [
    path('avatar/', AvatarViewSet.as_view({'get': 'get', 'post': 'create'}), name='avatar'),
    path('', ProfileViewSet.as_view({'get': 'get', 'post': 'create'}), name='profile'),
]
