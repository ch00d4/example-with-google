from django.contrib.auth.models import User


class UserService:

    def create_user(email, username=None, first_name=None, last_name=None, password=None):
        username = email if username is None else username
        user, created = User.objects.get_or_create(username=username, email=email)
        if first_name:
            user.first_name = first_name

        if last_name:
            user.last_name = last_name

        if password:
            user.set_password(password)

        user.save()
        return user, created
