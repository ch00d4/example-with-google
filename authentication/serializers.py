from django.contrib.auth.models import User

from rest_framework import serializers


class UserLoginSocialSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('pk', 'username', 'email', 'first_name', 'last_name')


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'last_name', 'groups')
        read_only_fields = ('id', 'email', 'first_name', 'last_name', 'groups')


class LoginSocialGoogleSerializer(serializers.Serializer):
    access_token = serializers.CharField(max_length=512)
    id_token = serializers.CharField(max_length=512)


class LoginSocialGoogleResponseSerializer(serializers.Serializer):
    user = UserLoginSocialSerializer()
    access_token = serializers.CharField(max_length=512)
    refresh_token = serializers.CharField(max_length=512)
