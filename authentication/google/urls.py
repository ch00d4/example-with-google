from django.urls import path

from .views import GoogleAuthViewSet

urlpatterns = [
    path('google/', GoogleAuthViewSet.as_view({'post': 'create'}), name='google-auth')
]
