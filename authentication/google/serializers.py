from rest_framework import serializers

from users.serializers import UserModelResponseSerializer


class GoogleAuthRequestSerializer(serializers.Serializer):
    access_token = serializers.CharField()


class GoogleAuthResponseSerializer(serializers.Serializer):
    access_token = serializers.CharField()
    refresh_token = serializers.CharField()
    user = UserModelResponseSerializer()
