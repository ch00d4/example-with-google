import requests

from authentication.service import UserService

from .models import GoogleProfile


class GoogleAuthService:
    TOKEN = None
    STATUS_CODE = None
    RESPONSE = None
    USER = None
    URL = 'https://www.googleapis.com/userinfo/v2/me'

    def __init__(self, token):
        self.TOKEN = token

    def _get_user_info(self):
        headers = {'Authorization': f'Bearer {self.TOKEN}'}
        response = requests.request('get', self.URL, data='', headers=headers)
        self.STATUS_CODE = response.status_code
        self.RESPONSE = response.json()
        if self.STATUS_CODE == 200:
            data = self.RESPONSE
            if data.get('email', '') == '':
                self.STATUS_CODE = 401
                self.RESPONSE = {
                    'error': {
                        'code': 401,
                        'message': 'google response not have email',
                        'status': 'UNAUTHENTICATED'
                    }
                }
            else:
                self.USER, _ = UserService.create_user(
                    email=data.get('email'),
                    first_name=data.get('given_name'),
                    last_name=data.get('family_name')
                )
                GoogleProfile.objects.get_or_create(id_google=data.get('id'), user=self.USER)

    def get_errors(self):
        return self.RESPONSE['error'] if self.STATUS_CODE != 200 else None

    def get_user(self):
        return self.USER

    def validate(self):
        self._get_user_info()
        if self.STATUS_CODE == 200:
            return True

        return False
