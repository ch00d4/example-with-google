from drf_spectacular.views import extend_schema
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_framework_simplejwt.tokens import RefreshToken

from users.serializers import UserModelResponseSerializer

from .models import GoogleProfile
from .serializers import (
    GoogleAuthRequestSerializer,
    GoogleAuthResponseSerializer
)
from .service import GoogleAuthService


class GoogleAuthViewSet(GenericViewSet):
    permission_classes = (AllowAny, )
    queryset = GoogleProfile.objects.none()
    serializer_class = GoogleAuthRequestSerializer

    @extend_schema(
        request=GoogleAuthRequestSerializer,
        responses=GoogleAuthResponseSerializer
    )
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer_class()
        serializer = serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.data
        google_auth = GoogleAuthService(data['access_token'])
        if google_auth.validate():
            tokens = RefreshToken.for_user(google_auth.get_user())
            serializer_user = UserModelResponseSerializer(instance=google_auth.get_user())
            return Response(data={
                'refresh_token': str(tokens),
                'access_token': str(tokens.access_token),
                'user': serializer_user.data
            })
        else:
            return Response(google_auth.get_errors(), status=status.HTTP_400_BAD_REQUEST)
