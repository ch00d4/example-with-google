from django.contrib.auth.models import User
from django.db import models


class GoogleProfile(models.Model):
    id_google = models.TextField(unique=True, primary_key=True)
    picture = models.TextField()
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    class Meta:
        db_table = 'auth_user_google'
